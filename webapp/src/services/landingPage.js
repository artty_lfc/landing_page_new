const connection = require('../db/db.js');
var emoji = require('node-emoji')
const {
    TwingEnvironment,
    TwingLoaderFilesystem
} = require('twing');
let loader = new TwingLoaderFilesystem('./templates');
let twing = new TwingEnvironment(loader);

module.exports = {
    getData: (id) => {
        return new Promise(function (resolve, reject) {
            var sqlGallery = "SELECT * FROM manage_gallery where id=" + id
            connection.query(sqlGallery, (error, results, fields) => {
                var sqlGallery2 = "SELECT * FROM manage_images where id_gallery=" + id
                connection.query(sqlGallery2, (error, results2, fields) => {
                    var sqlGallery3 = "SELECT * FROM manage_video where id_gallery=" + id
                    connection.query(sqlGallery3, (error, results3, fields) => {
                        var convertEmoji = emoji.emojify(results[0].description)
                        var convertEmojiTitle = emoji.emojify(results[0].title)
                        var dataItem = {
                            id: results[0].id,
                            title: convertEmojiTitle,
                            description: convertEmoji,
                            status: results[0].status,
                            timestamp: results[0].timestamp,
                            datetime: results[0].datetime,
                            imagelist: results2,
                            videolist: results3
                        }
                        // console.log("results2====", results2)
                        resolve(dataItem)
                    })
                });
            })
        })
    },
    saverRefLink: (ref_link, coach_id, line, facebook, product_id) => {
        return new Promise(function (resolve, reject) {
            let sqlCheck = "SELECT coach_id FROM `manage_coach` WHERE coach_id = " + coach_id + " "
            let sql = "INSERT INTO `manage_ref_link`(`ref_link`, `coach_id`,`line`,`facebook`) VALUES ('" + ref_link + "'," + coach_id + ",'" + line + "','" + facebook + "')"
            let sqlCoach = "INSERT INTO `manage_coach`(`coach_id`, `product_id`) VALUES (" + coach_id + ",'" + product_id + "')"
            let sqlCheckRef = "SELECT ref_link FROM `manage_ref_link` WHERE ref_link = '" + ref_link + "' "
            console.log(sql)
            connection.query(sqlCheck, (error, results, fields) => {
                if (!results.length) {
                    connection.query(sqlCoach, (error, results, fields) => {
                        connection.query(sql, (error, results, fields) => {
                            console.log("Success")
                        });
                    });
                } else {
                    connection.query(sqlCheckRef, (error, results, fields) => {
                        if (!results.length) {
                            connection.query(sql, (error, results, fields) => {
                                console.log("Success")
                            });
                        }
                    });
                }
            });
        })
    },
    getDataRefLink: (refCode) => {
        return new Promise(function (resolve, reject) {
            var sql = "SELECT ml.id,ml.ref_link,ml.coach_id,ml.line,ml.facebook,mc.product_id FROM manage_ref_link ml LEFT JOIN manage_coach mc ON mc.coach_id  = ml.coach_id WHERE ref_link = '" + refCode + "' "
            connection.query(sql, (error, results, fields) => {
                // console.log("results", results)
                if (!results.length) {
                    resolve(false)
                } else if (results[0].product_id == "[]") {
                    resolve(true)
                } else {
                    resolve(results)
                }
            });
        })
    },
    getDataImg: (img) => {
        return new Promise(function (resolve, reject) {
            var sql = "SELECT * FROM `manage_images` WHERE id_gallery = " + img + " "
            connection.query(sql, (error, results, fields) => {
                if (!results.length) {
                    resolve(false)
                } else {
                    resolve(results)
                }
            });
        })
    },
}