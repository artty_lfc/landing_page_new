const path = require("path");
const express = require('express');
const emojiRegex = require('emoji-regex');
const regex = emojiRegex();
var emoji = require('node-emoji')
const landingPage = require('./services/landingPage.js');

const app = express();
const {
    TwingEnvironment,
    TwingLoaderFilesystem
} = require('twing');
let loader = new TwingLoaderFilesystem('./templates');
let twing = new TwingEnvironment(loader);
const connection = require('./db/db.js');

app.use(express.static('public'))
app.use(express.json());

function loadShopInfo(refCode) {
    if (refCode != 'abc') {
        return null;
    }
}
app.get("/product", function (req, res) {
    console.log(req.query.recId);
    let refCode = req.query.ref_link;
    console.log('refCode : ', refCode)
    var sqlGallery = "SELECT * FROM manage_images mi LEFT JOIN manage_gallery mg ON mi.id_gallery = mg.id WHERE mi.id = " + req.query.recId + ""
    connection.query(sqlGallery, (error, results, fields) => {
        var emojiDes = emoji.emojify(results[0].description)
        var emojiTitle = emoji.emojify(results[0].title_img)
        var data = {
            "id": results[0].id,
            "images": results[0].images,
            "images_resize": results[0].images_resize,
            "title_img": emojiTitle,
            "description": emojiDes,
            "datetime": results[0].datetime,
            "id_gallery": results[0].id_gallery,
            "price": results[0].price,
            "status": results[0].status,
            "timestamp": results[0].timestamp
        }
        var img = results[0].id_gallery
        landingPage.getDataRefLink(refCode).then(function (dataRefCode) {
            landingPage.getDataImg(img).then(function (dataImg) {
                twing.render('full_image.twig', {
                    'refCoach': dataRefCode,
                    'dataGallery': data,
                    'img': dataImg
                }).then((output) => {
                    // res.json(resultTest)
                    res.end(output);
                });
            })
        })
    })

});


app.get("/:refCode", function (req, res) {
    let refCode = req.params.refCode;
    landingPage.getDataRefLink(refCode).then(function (dataRefCode) {
        if (dataRefCode == false) {
            twing.render('404.html').then((output) => {
                res.end(output);
            });
        } else if (dataRefCode == true) {
            twing.render('945no.html').then((output) => {
                res.end(output);
            });
        } else {
            var dataCoach;
            if (dataRefCode[0].line == "") {
                dataCoach = {
                    "id": dataRefCode[0].id,
                    "ref_link": dataRefCode[0].ref_link,
                    "coach_id": dataRefCode[0].coach_id,
                    "product_id": dataRefCode[0].product_id
                }
            } else {
                dataCoach = dataRefCode
            }
            console.log("Line", dataCoach)
            var product_id = dataRefCode[0].product_id
            var obj_id = JSON.parse(product_id)
            var data = [];
            obj_id.forEach(function (entry) {
                var id = entry;
                landingPage.getData(id).then(function (dataResult) {
                    data.push(dataResult);
                    if (data.length == obj_id.length) {
                        // console.log(data);
                        twing.render('index.twig', {
                            'refCoach': dataCoach,
                            'dataGallery': data
                        }).then((output) => {
                            res.end(output);
                        });
                    }
                })
            });
        }
    })
});

app.listen(3000);