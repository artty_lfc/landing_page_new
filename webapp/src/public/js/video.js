$(function() {
    var slider = $(".royalSlider").royalSlider({
        arrowsNav: false,
        fadeinLoadedSlide: true,
        controlNavigationSpacing: 0,
        controlNavigation: 'thumbnails',
        thumbs: {
            autoCenter: false,
            fitInViewport: true,
            orientation: 'vertical',
            spacing: 0,
            paddingBottom: 0,
            spacing: 10,
        },
        keyboardNavEnabled: false,
        imageScaleMode: 'fill',
        imageAlignCenter: true,
        slidesSpacing: 0,
        loop: false,
        loopRewind: true,
        numImagesToPreload: 3,
        video: {
            // autoHideArrows: true,
            // autoHideControlNav: false,
            // autoHideBlocks: true
            autoHideBlocks: true,
            autoHideArrows: false
        },
        autoScaleSlider: true,
        autoScaleSliderWidth: 960,
        autoScaleSliderHeight: 450,
        imgWidth: 640,
        imgHeight: 360,

    }).data('royalSlider');
});